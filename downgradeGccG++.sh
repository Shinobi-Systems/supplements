#!/bin/sh
if [ -x "$(command -v apt)" ]; then
    sudo apt-get install gcc-7 g++-7 -y
    rm /usr/bin/gcc
    rm /usr/bin/g++

    ln -s /usr/bin/gcc-7 /usr/bin/gcc
    ln -s /usr/bin/g++-7 /usr/bin/g++
fi
# Check if Cent OS
if [ -x "$(command -v yum)" ]; then
    yum install centos-release-scl
    yum install devtoolset-7-gcc-c++
fi
