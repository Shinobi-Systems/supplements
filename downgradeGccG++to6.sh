#!/bin/sh
if [ -x "$(command -v apt)" ]; then
    sudo apt-get install gcc-6 g++-6 -y
    rm /usr/bin/gcc
    rm /usr/bin/g++

    ln -s /usr/bin/gcc-6 /usr/bin/gcc
    ln -s /usr/bin/g++-6 /usr/bin/g++
fi
# Check if Cent OS
if [ -x "$(command -v yum)" ]; then
    yum install centos-release-scl
    yum install devtoolset-6-gcc-c++
fi
