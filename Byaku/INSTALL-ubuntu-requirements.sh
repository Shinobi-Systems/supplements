sudo apt install imagemagick -y
sudo apt install build-essential -y
sudo apt install gcc-7 g++-7 -y
sudo bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/downgradeGccG++.sh)
if [ -x "$(command -v nvidia-smi)" ]; then
    sudo bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/cudaInstall.sh)
fi
